@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @foreach($s as $data)
            <div class="card">
                <div class="card-header">
                <img src="{{asset($data->avatar)}}" class="rounded-circle" alt="love" width="5%" height="40%">&ensp;
                {{$data->name}}
                </div>

                <div class="card-body">
                <img src="{{asset($data->image)}}" class="card-img-too" alt="love" width="690" height="850">&ensp;
                <i class="fa fa-heart" style="color:pink;"></i>
                <i class="fa fa-comment"></i> <br>
                {{$data->email}} <br>
                {{$data->caption}}
                <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Add a comment" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                <a href="#"> <span class="input-group-text" id="basic-addon2">Post</span> </a>
                </div>
                </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
