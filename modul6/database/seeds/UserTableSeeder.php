<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'neila',
            'email' => 'neilaa@gmail.com',
            'password' => bcrypt('1234567890'),
            'avatar'=>'img/21.jpg',
        ]);
    }
}